Mail Alterer
------------
This provides a simple mechanism for overriding values in emails being delivered
by the website.

The primary intended use is to replace URLs from an admin-only instance of the
website to the site's primary URL.


Configuration
--------------------------------------------------------------------------------
Edit the settings.php file and create a variable named "mail_alterer_alters".
There should be one element in this array for the main attributes of Drupal's
email system that needs to be modified; currently only "subject", "body" and
"headers" are supported.

// Mail Alterer:
$conf['mail_alterer_alters'] = array(
  'subject' => array(
    // Fix an old goof where someone wrote "chickens" instead of "chocolate".
    'chickens' => 'chocolate',
  ),
  'body' => array(
    // Make sure no emails are delivered with links to the demo site.
    'https://demo.example.com' => 'https://www.example.com',
    // Per company policy, make sure all links are secure.
    'http://' => 'https://',
    // Fix an old goof where someone wrote "potatoe" instead of "potato".
    'potatoe' => 'potato',
    'Potatoe' => 'Potato',
  ),
  'headers' => array(
    // Fix an old goof where someone wrote "hamster" instead of "user".
    'Hamster' => 'User',
    'hamster' => 'user',
  ),
);

See the official documentation for hook_mail_alter() for further details:
https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_mail_alter/7.x


Credits / contact
--------------------------------------------------------------------------------
Written and maintained by Damien McKenna [1].

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://www.drupal.org/project/issues/mail_alterer


References
--------------------------------------------------------------------------------
1: https://www.drupal.org/u/damienmckenna
